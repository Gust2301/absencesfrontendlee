import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CahierAbsenceComponent} from './cahier-absence/cahier-absence.component';
const routes: Routes = [
  { path: '', component: CahierAbsenceComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
