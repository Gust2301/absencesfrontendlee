import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CahierAbsenceComponent } from './cahier-absence/cahier-absence.component';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import {
	IgxGridModule,
	IgxFocusModule
 } from "igniteui-angular";
@NgModule({
  declarations: [
    AppComponent,
    CahierAbsenceComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    AppRoutingModule,
    IgxGridModule.forRoot(),
    IgxFocusModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
