import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  show: boolean = false;
  mat=""
  title = 'Gestion des absences M2PLS';
  classes: string[] = [
    'M2PLS', 'M2EID'
  ];
  matieres: string[] = [
    'LEE', 'Java Srcipt','Surete et Securite', 'EDL'
  ];
  setMatiere(){
    this.show = true
  }
}
