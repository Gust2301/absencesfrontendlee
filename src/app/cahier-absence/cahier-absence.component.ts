import { Component, OnInit,ViewChild } from '@angular/core';
import { data } from "./data";

import { IgxGridComponent } from "igniteui-angular";

@Component({
  selector: 'app-cahier-absence',
  templateUrl: './cahier-absence.component.html',
  styleUrls: ['./cahier-absence.component.scss']
})
export class CahierAbsenceComponent implements OnInit {

  @ViewChild("gridRowEdit", { read: IgxGridComponent }) public gridRowEdit: IgxGridComponent;
  now = new Date();
  mois    = this.now.getMonth() + 1;
  dateA = this.now.getDate()+"/"+this.mois+"/"+this.now.getFullYear();
  public data: any[];

  constructor() {
      this.data = data;
  }

  ngOnInit() {
  }

}
