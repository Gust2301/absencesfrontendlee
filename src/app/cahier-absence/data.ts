export const data = [{
    ID: 1,
    absent: false,
    NumEtu: "11710087",
    nom: "VARORE",
    prenom: "Augustin"
}, {
  ID: 2,
  absent: false,
  NumEtu: "11710088",
  nom: "NDIAYE",
  prenom: "Sega"
}, {
  ID: 3,
  absent: false,
  NumEtu: "11610057",
  nom: "BENKEMIS",
  prenom: "Sofiane"
},{
  ID: 4,
  absent: false,
  NumEtu: "11710485",
  nom: "MADOUANI",
  prenom: "Yacine"
}, {
  ID: 5,
  absent: false,
  NumEtu: "11710012",
  nom: "BA",
  prenom: "Alpha"
}, {
  ID: 6,
  absent: false,
  NumEtu: "11714785",
  nom: "CHIBOUTI",
  prenom: "Boualem"
},{
  ID: 7,
  absent: false,
  NumEtu: "11715472",
  nom: "TORQUI",
  prenom: "Soumaya"
},{
  ID: 8,
  absent: false,
  NumEtu: "11123087",
  nom: "SLIMANI",
  prenom: "ILHAM"
},{
  ID: 9,
  absent: false,
  NumEtu: "11710041",
  nom: "RAJASPERA",
  prenom: "Cynthia"
},{
  ID: 10,
  absent: false,
  NumEtu: "11247563",
  nom: "REKIK",
  prenom: "Fadwa"
}];
