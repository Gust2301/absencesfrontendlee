import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CahierAbsenceComponent } from './cahier-absence.component';

describe('CahierAbsenceComponent', () => {
  let component: CahierAbsenceComponent;
  let fixture: ComponentFixture<CahierAbsenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CahierAbsenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CahierAbsenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
